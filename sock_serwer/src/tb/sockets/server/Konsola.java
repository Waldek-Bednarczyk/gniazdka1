package tb.sockets.server;

import java.io.IOException;
import java.net.ServerSocket;

public class Konsola {

	public static void main(String[] args) {
		try {
			ServerSocket sSock = new ServerSocket(6666);
			while (true) {
                            Game game = new Game();
                            Game.Player playerX = game.new Player(sSock.accept(), 'X');
                            Game.Player playerO = game.new Player(sSock.accept(), 'O');
                            playerX.setOpponent(playerO);
                            playerO.setOpponent(playerX);
                            game.currentPlayer = playerX;
                            playerX.start();
                            playerO.start();
                        }
                    } 
                catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
