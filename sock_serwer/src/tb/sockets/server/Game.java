package tb.sockets.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Game {

    private final Player[] field = {null, null, null,null, null, null,null, null, null};
    Player currentPlayer;

    public boolean win() {
        return
            (field[0] != null && field[0] == field[1] && field[0] == field[2])
          ||(field[3] != null && field[3] == field[4] && field[3] == field[5])
          ||(field[6] != null && field[6] == field[7] && field[6] == field[8])
          ||(field[0] != null && field[0] == field[3] && field[0] == field[6])
          ||(field[1] != null && field[1] == field[4] && field[1] == field[7])
          ||(field[2] != null && field[2] == field[5] && field[2] == field[8])
          ||(field[0] != null && field[0] == field[4] && field[0] == field[8])
          ||(field[2] != null && field[2] == field[4] && field[2] == field[6]);
    }

    public boolean fieldFull() {
        for (Player field1 : field) {
            if (field1 == null) {
                return false;
            }
        }
        return true;
    }

    public synchronized boolean played(int location, Player player) {
        if (player == currentPlayer && field[location] == null) {
            field[location] = currentPlayer;
            currentPlayer = currentPlayer.opponent;
            currentPlayer.otherPlayerMoved(location);
            return true;
        }
        return false;
    }

    class Player extends Thread {
        char sign;
        Player opponent;
        Socket socket;
        BufferedReader input;
        PrintWriter output;

        public Player(Socket socket, char sign) {
            this.socket = socket;
            this.sign = sign;
            try {
                input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
                output = new PrintWriter(socket.getOutputStream(), true);
                output.println("WELCOME " + sign);
                output.println("MESSAGE Oczekiwanie na połączenie");
            } catch (IOException e) {
                System.out.println("Zerwano połączenie: " + e);
            }
        }

        public void setOpponent(Player opponent) {
            this.opponent = opponent;
        }

        public void otherPlayerMoved(int location) {
            output.println("OPPONENT_MOVED " + location);
            output.println(win() ? "DEFEAT" : fieldFull() ? "TIE" : "");
        }

        @Override
        public void run() {
            try {
                output.println("MESSAGE Możemy zaczynać grę");

                if (sign == 'X') {
                    output.println("MESSAGE Twój ruch");
                }

                while (true) {
                    String command = input.readLine();
                    if (command.startsWith("MOVE")) {
                        int location = Integer.parseInt(command.substring(5));
                        if (played(location, this)) {
                            output.println("VALID_MOVE");
                            output.println(win() ? "VICTORY"
                                         : fieldFull() ? "TIE"
                                         : "");
                        } else {
                            output.println("MESSAGE ?");
                        }
                    } else if (command.startsWith("QUIT")) {
                        return;
                    }
                }
            } catch (IOException e) {
                System.out.println("Zerwano połączenie: " + e);
            } finally {
                try {socket.close();} catch (IOException e) {}
            }
        }
    }
}