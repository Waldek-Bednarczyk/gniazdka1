package tb.sockets.client;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import static javax.swing.SpringLayout.NORTH;

public class GameClient {
    private final JFrame frame = new JFrame("Kółko i Krzyżyk");
    private final JLabel messageLabel = new JLabel("");
    private ImageIcon icon;
    private ImageIcon opponentIcon;
    private Field[] board = new Field[9];
    private Field currentSquare;
    private static final int PORT = 6666;
    private final Socket socket;
    private final BufferedReader in;
    private PrintWriter out;

    public GameClient(String serverAddress) throws Exception {

        socket = new Socket(serverAddress, PORT);
        in = new BufferedReader(new InputStreamReader(
            socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        messageLabel.setBackground(Color.lightGray);
        frame.getContentPane().add(messageLabel, NORTH);

        JPanel boardPanel = new JPanel();
        boardPanel.setBackground(Color.blue);
        boardPanel.setLayout(new GridLayout(3, 3, 2, 2));
        for (int i = 0; i < board.length; i++) {
            final int j = i;
            board[i] = new Field();
            board[i].addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    currentSquare = board[j];
                    out.println("MOVE " + j);}});
            boardPanel.add(board[i]);
        }
        frame.getContentPane().add(boardPanel, "Center");
    }

    public void play() throws Exception {
        String response;
        try {
            response = in.readLine();
            if (response.startsWith("WELCOME")) {
                char mark = response.charAt(8);
                icon = new ImageIcon(mark == 'X' ? "krzyzyk.png" : "kolko.png");
                opponentIcon  = new ImageIcon(mark == 'X' ? "kolko.png" : "krzyzyk.png");
                frame.setTitle("Gracz " + mark);
            }
            while (true) {
                response = in.readLine();
                if (response.startsWith("VALID_MOVE")) {
                    messageLabel.setText("Proszę czekać");
                    currentSquare.setIcon(icon);
                    currentSquare.repaint();
                } else if (response.startsWith("OPPONENT_MOVED")) {
                    int loc = Integer.parseInt(response.substring(15));
                    board[loc].setIcon(opponentIcon);
                    board[loc].repaint();
                    messageLabel.setText("Twój ruch");
                } else if (response.startsWith("VICTORY")) {
                    messageLabel.setText("Wygrałeś!");
                    break;
                } else if (response.startsWith("DEFEAT")) {
                    messageLabel.setText("Przegrałeś :(");
                    break;
                } else if (response.startsWith("TIE")) {
                    messageLabel.setText("Remis :|");
                    break;
                } else if (response.startsWith("MESSAGE")) {
                    messageLabel.setText(response.substring(8));
                }
            }
            out.println("QUIT");
        }
        finally {
            socket.close();
        }
    }

    private boolean repeat() {
        int response = JOptionPane.showConfirmDialog(frame, "Jeszcze raz?",
                "Kółko i Krzyżyk", JOptionPane.YES_NO_OPTION);
        frame.dispose();
        return response == JOptionPane.YES_OPTION;
    }

    static class Field extends JPanel {
        JLabel label = new JLabel((Icon)null);
        public Field() {
            setBackground(Color.white);
            add(label);
        }
        public void setIcon(Icon icon) {
            label.setIcon(icon);
        }
    }
        public static void main(String[] args) throws Exception {
        while (true) {
            String serverAddress = (args.length == 0) ? "localhost" : args[1];
            GameClient client = new GameClient(serverAddress);
            client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            client.frame.setSize(600, 400);
            client.frame.setVisible(true);
            client.frame.setResizable(false);
            client.play();
            if (!client.repeat()) {
                break;
            }
        }
    }
}